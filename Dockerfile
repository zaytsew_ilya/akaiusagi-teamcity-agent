FROM dockerfile/java
MAINTAINER Ilya Zaytsev <zaytsew.ilya@gmail.com>

ENV INSTALL_PATH /root/buildAgent
ENV AGENT_NAME default_agent
ENV SERVER_URL http://192.168.0.2:8111

RUN mkdir $INSTALL_PATH
RUN wget -O /root/buildAgent.zip $SERVER_URL/update/buildAgent.zip
RUN unzip /root/buildAgent.zip -d $INSTALL_PATH
RUN rm -rf /root/buildAgent.zip

RUN cp $INSTALL_PATH/conf/buildAgent.dist.properties $INSTALL_PATH/conf/buildAgent.properties
RUN sed -i -e "s/^name.*=.*/name=$AGENT_NAME/" -e "s/^serverUrl.*=.*/serverUrl=$(echo $SERVER_URL | sed "s/\//\\\\\//g")/" $INSTALL_PATH/conf/buildAgent.properties

RUN chmod +x /root/buildAgent/bin/agent.sh

EXPOSE 9090

CMD $INSTALL_PATH/bin/agent.sh run


