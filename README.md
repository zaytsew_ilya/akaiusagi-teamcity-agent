## Dockerfile for teamcity agent


### Dependencies

* [dockerfile/java](http://dockerfile.github.io/#/java)

### Build

    docker build -t akaiusagi/teamcity-agent .


### Usage

    docker run -d -p 9090:9090 akaiusagi/teamcity-agent
